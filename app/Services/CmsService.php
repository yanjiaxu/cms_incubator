<?php
/**
 * Created by PhpStorm.
 * User: samxiao
 * Date: 2018/7/6
 * Time: 下午2:44
 */

namespace App\Services;


use App\Models\CMS\Tag;

class CmsService
{
    public function asyncTags($tags) {
        foreach ($tags as $item) {
            $tag = Tag::query()->where('name', $item)->first();
            if (!$tag) {
                $tag = new Tag();
            }
            $tag->name = $item;
            $tag->save();
        }
    }
}
