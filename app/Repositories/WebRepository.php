<?php

namespace App\Repositories;
use Illuminate\Support\Facades\DB;

class WebRepository
{
    public static function getNav()
    {
      return  DB::table('cms_navigations')->whereNull('deleted_at')->get();
    }
}
