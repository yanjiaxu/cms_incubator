<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class FileController extends Controller
{
    /**
     * 上传文件接口
     * @param Request $request
     * @return mixed
     */
    public function fileUpload(Request $request)
    {
        try {
            $file = $request->file('file');
            $_path = md5(uniqid(microtime(true), true));
            $path = 'assets/' . $_path;
            $savePath = Storage::put($path, $file);
            $path = ['path' => Storage::url($savePath)];
            return response($path, 200);
        } catch (\Exception $e) {
            return  response()->json($e,462);
        }
    }
}
