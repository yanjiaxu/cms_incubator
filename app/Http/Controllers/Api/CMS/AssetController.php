<?php

namespace App\Http\Controllers\Api\CMS;

use App\Http\Requests\AssetRequest;
use App\Models\CMS\Assets;
use App\Models\CMS\Blog;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\AssetsResource;
use Illuminate\Support\Facades\Storage;
use  App\Services\CmsService;

class AssetController extends Controller
{
    public $cmsService;

    public function __construct()
    {
        $this->cmsService = new CmsService();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $type = $request->get('type', '');
        $querys = Assets::query()->orderByDesc('id');
        if ($type != 'all') {
            $querys->when($type, function ($query) use ($type) {
                return $query->where('type', $type);
            });
        }
        $model = $querys->paginate(18);
        return AssetsResource::collection($model);
    }

    /**
     * @param AssetRequest $request
     * @return AssetsResource
     */
    public function store(Request $request)
    {
        try {
            $file = $request->file('file');
            $name = $file->getClientOriginalName();
            $nameEx = pathinfo($name)['extension'];
            $nameStr = strtolower($nameEx);
            $_path = md5(uniqid(microtime(true), true));
            $fileName = pathinfo($name)['filename'];
            $path = 'assets/' . $_path;

            $savePath = Storage::put($path, $file);
            $model = new Assets();
            $model->path = Storage::url($savePath);
            switch ($nameStr) {
                case 'jpg':
                case 'png':
                case 'jpeg':
                    $model->type = 'image';
                    break;
                case 'mp4':
//                case 'avi':
//                case 'wmv':
//                case 'rmvb':
                    $model->type = 'video';
                    break;
                default:
                    $model->type = 'file';
            }
            $model->name = $fileName;
            $model->save();
            return new AssetsResource($model);
        } catch (\Exception $e) {
           return  response()->json($e,462);
        }

    }


    /**
     * @param $assetId
     * @return AssetsResource
     */
    public function show($assetId)
    {
        $model = $this->findModel($assetId);
        return new AssetsResource($model);
    }

    /**
     * @param Request $request 编辑
     * @param $assetId
     * @return AssetsResource
     */
    public function update(Request $request, $assetId)
    {
        $name = $request->get('name');
        $tags = $request->get('tags');
        $this->cmsService->asyncTags($tags);
        $tagsJson = json_encode($tags,true);
        $model = $this->findModel($assetId);
        $model->name = $name;
        $model->tags = $tagsJson;
        $model->save();
        return new AssetsResource($model);
    }

    /**
     * @param $assetId  删除
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy($assetId)
    {
        $model = $this->findModel($assetId);
        Storage::delete($model->path);
        Storage::delete($model->thumbnail_path);
        $model->delete();
        return response()->json(['state' => 'success']);
    }

    /**
     * @param $assetId
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
     */
    public function findModel($assetId)
    {
        $model = Assets::query()->findOrFail($assetId);
        return $model;
    }
}
