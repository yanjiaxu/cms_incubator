<?php
/**
 * Created by PhpStorm.
 * User: MI
 * Date: 2019/3/6
 * Time: 14:20
 */

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Http\Resources\ContentResource;
use App\Models\Cms\Content;
use App\Models\CMS\Tag;
use App\Models\FinancialInstitution\FinancialInstitution;
use App\Models\MessageTemplateItem;
use App\Models\Permission;
use App\Models\Role;
use App\Http\Resources\SelectorResource;
use Illuminate\Http\Request;

class DataProviderController extends Controller
{
    /**
     * 全部权限
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function getPermission()
    {
        $models = Permission::query()->where(['guard_name'=>'admin'])->get();
        return SelectorResource::collection($models);
    }

    /**
     * 全部角色
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function getRole()
    {
        $models = Role::query()->where(['guard_name'=>'admin'])->get();
        return SelectorResource::collection($models);
    }

    public function getTag(){
        $models = Tag::query()->get();
        return SelectorResource::collection($models);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function getContentsByCategory(Request $request){
        $category = $request->get('category');
        $category = end($category);
        $content = Content::query()->with('content_detail')->whereHas('categorylabel', function ($query) use ($category) {
                    $query->where('category_path', 'like',"%-$category-%");
                })->get();
        return ContentResource::collection($content);
    }
}
