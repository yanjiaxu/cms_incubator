<?php
/**
 * Created by PhpStorm.
 * User: MI
 * Date: 2019/2/12
 * Time: 15:38
 */

namespace App\Http\Controllers\Site;


//use aliyun\live\Client;
use App\Http\Controllers\Controller;

class SiteController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('home');
    }

    public function about()
    {
        return view('about');
    }

    public function team()
    {
        return view('team');
    }

}
