<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AssetRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
//        return $this->user->can('');
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
           // 'name' => 'required',
            'file' => 'required|file',
//            'type' => 'required',
            //'thumbnail_path'=>'required',
        ];
    }

    public function messages()
    {
        return [
           // 'name.required' => '素材名称必填',
            'file.required' => '请上传素材',
            'file.file' => '请上传素材',
//            'type.required' => '素材类型必填',
            // 'thumbnail_path.required'=>'素材thumb路径必填',
        ];
    }
}
