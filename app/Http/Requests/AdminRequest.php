<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class AdminRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username'=> [
                'required',
                Rule::unique('admins')
                    ->whereNot('id',$this->id)
            ],
        ];
    }
    public function messages()
    {
        return [
            'username.required' => '用户名不能为空.',
            'username.unique'=>'该用户名已被注册'
        ];
    }
}
