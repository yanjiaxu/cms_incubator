<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RoleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=> [
                'required',
                Rule::unique('roles')
                    ->where('guard_name','admin')
                    ->whereNot('id',$this->id)
            ],
        ];
    }
    public function messages()
    {
        return [
            'name.required' => '名称不能为空.',
            'name.unique'=>'该名称已被注册'
        ];
    }
}
