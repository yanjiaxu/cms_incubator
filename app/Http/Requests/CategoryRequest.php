<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
//        return $this->user->can('');
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('category');
        if (!$id) {
            $id = 0;
        }

        return [
            'name' => [
                'required'
            ],
        ];
    }

    public function messages()
    {
        return [
            'name.required' => '栏目必填',
        ];
    }
}
