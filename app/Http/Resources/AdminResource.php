<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AdminResource extends JsonResource
{
    use BaseResource;
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $return = parent::toArray($request);
        $return['roles'] = $this->hasInclude($request,'info')?$this->getAllPermissions()->pluck('name'):'';
        $return['permissions'] = $this->hasInclude($request,'update')?$this->roles->pluck('id')[0]:'';
        return  $return;

    }
}
