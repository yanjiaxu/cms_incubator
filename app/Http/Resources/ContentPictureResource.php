<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ContentPictureResource extends JsonResource
{
    use BaseResource;
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'assets_id'=>$this->assets_id,
            'title'=>$this->title,
            'point_type' => (int)$this->point_type,
            'point_content_id' => (int)$this->point_content_id,
            'point_category_id' => $this->StringArrayToInt(explode("-",$this->point_category_path)),
            'point_url' => (string)$this->point_url,
            'thumbnail_path'=>(string)$this->asset?$this->asset->path:'',
            'tags'=>$this->hasInclude($request,'update')?json_decode($this->contents->tags,true):'',
            'category'=>$this->hasInclude($request,'update')?$this->contents->getCategory():'',
            'categories'=>$this->hasInclude($request,'index')?$this->contents->category:'',
        ];
    }
}
