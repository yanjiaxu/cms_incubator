<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class NavigationResource extends JsonResource
{
    use BaseResource;
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => (int)$this->id,
            'name' => (string)$this->name,
            'point_type' => (int)$this->point_type,
            'point_content_id' => (int)$this->point_content_id,
            'point_category_id' => $this->StringArrayToInt(explode("-",$this->point_category_path)),
            'point_url' => (string)$this->point_url,
            'sort'=>$this->sort
        ];
    }
}
