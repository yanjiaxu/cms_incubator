<?php
/**
 * Created by PhpStorm.
 * User: MI
 * Date: 2019/7/16
 * Time: 10:21
 */

namespace App\Http\Resources;


use Carbon\Carbon;
use Illuminate\Http\Resources\Json\Resource;

class AssetsResource extends Resource
{
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'name'=>$this->name,
            'tags'=>json_decode($this->tags,true),
            'path'=>$this->path,
            'created_at'=>$this->created_at->toDateTimeString()
        ];
    }
}
