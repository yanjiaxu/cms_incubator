<?php

namespace App\Http\Resources;

use App\Http\Resources\Cms\Content;
use Illuminate\Http\Resources\Json\JsonResource;

class ContentBlogResource extends JsonResource
{
    use BaseResource;
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => (int)$this->id,
            'cover_image' => (int)$this->cover_image,
            'title'=>$this->title,
            'sub_title'=>$this->sub_title,
            'content'=>$this->content,
            'thumbnail_path'=>(string)$this->asset?$this->asset->path:'',
            'tags'=>json_decode($this->contents->tags,true),
            'category'=>$this->hasInclude($request,'update')?$this->contents->getCategory():'',
            'categories'=>$this->hasInclude($request,'index')?$this->contents->category:'',
        ];
    }
}
