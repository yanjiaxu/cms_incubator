<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class SelectorResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        if (!empty($this->name)) {
            return [
                'label' => (string)((string)$this->name),
                'value' => (int)$this->id
            ];
        } elseif (!empty($this->username)) {
            return [
                'label' => $this->username,
                'value' => (int)$this->id
            ];
        }elseif (!empty($this->label)) {
            return [
                'label' => $this->label,
                'value' => $this->value
            ];
        }else{
            return [
                'label' => (string)$this->name,
                'value' => (int)$this->id
            ];
        }

    }
}
