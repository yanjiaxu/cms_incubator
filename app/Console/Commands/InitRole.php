<?php

namespace App\Console\Commands;

use App\Models\Admin;
use App\Models\FinancialInstitution\FinancialInstitutionAdmin;
use App\Models\Permission;
use App\Models\Role;
use Illuminate\Console\Command;

class InitRole extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'init:role';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '初始化用户权限';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $admin = Role::create(['name' => '管理员','guard_name'=>'admin']);
        /**
         * 用户管理
         */
        $permission = Permission::create(['name' => '用户管理','guard_name'=>'admin']);
        $permission->syncRoles($admin);
        /**
         * 权限管理
         */
        $permission = Permission::create(['name' => '权限管理','guard_name'=>'admin']);
        $permission->syncRoles($admin);
        /**
         * 管理员
         */
        $user = Admin::query()->where('username','admin')->first();
        $user->assignRole('管理员');
    }
}
