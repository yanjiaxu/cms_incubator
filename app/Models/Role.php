<?php

namespace App\Models;

use Spatie\Permission\Exceptions\RoleAlreadyExists;
use Spatie\Permission\Models\Role as RoleModel;

/**
 * Class Role
 * @package App\Models
 */
class Role extends RoleModel
{
    protected $fillable = [
        'guard_name',
        'name'
    ];
}
