<?php

namespace App\Models\CMS;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Navigation extends Model
{
    use SoftDeletes;

    //表名
    public $table = 'cms_navigations';

    public $fillable = [
        'name',
        'parent_id',
        'sort',
        'point_url',
        'point_type',
        'point_content_id',
        'point_category_id',
        'point_category_path'
    ];

    /**
     * 分类
     * @return mixed
     */
    public function category()
    {
        return $this->hasOne(Category::class, 'id', 'point_category_id')->withTrashed();
    }

    //内容
    public function content()
    {
        return $this->hasOne(Content::class, 'id', 'point_content_id')->withTrashed();
    }

    //父级分类
    public function parent()
    {
        return $this->belongsTo(Navigation::class, 'parent_id', 'id')->withTrashed();
    }

}
