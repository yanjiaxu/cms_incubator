<?php

namespace App\Models\CMS;

use Illuminate\Database\Eloquent\Model;

class Assets extends Model
{
    public $table = 'cms_assets';
    protected $fillable = [
        'name',
        'path',
        'thumbnail_path',
        'type',
        'tags'
    ];
}
