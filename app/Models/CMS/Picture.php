<?php

namespace App\Models\CMS;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Picture extends Model
{
    use SoftDeletes;
    /**
     * 对应表名
     * @var string
     */
    public $table = 'cms_content_pictures';

    /**
     * @var array
     */
    public $fillable = [
        'title',
        'assets_id',
        'content_id',
        'point_url',
        'point_type',
        'point_category_id',
        'point_content_id',
        'point_content_path',
        'description'
    ];

    public function contents()
    {
        return $this->morphOne(Content::class, 'content_detail');
    }

    public function asset()
    {
        return $this->hasOne(Assets::class, 'id', 'assets_id');
    }
}
