<?php

namespace App\Models\CMS;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    public $timestamps = false;
    protected $table = 'cms_tags';

    protected $fillable = [
        'name',
    ];
}
