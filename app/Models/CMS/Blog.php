<?php

namespace App\Models\CMS;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Blog extends Model
{
    use SoftDeletes;

    const CACHE_TAG = 'content-list';

    public $table = 'cms_content_blogs';

    //   public $timestamps = FALSE;

    public $fillable = [
        'content_id',
        'title',
        'content',
        'sub_title',
        'cover_image'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function contents()
    {
        return $this->morphOne(Content::class,'content_detail');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function content()
    {
        return $this->hasOne(Content::class, 'id', 'content_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function asset()
    {
        return $this->hasOne(Assets::class, 'id', 'cover_image');
    }
}
