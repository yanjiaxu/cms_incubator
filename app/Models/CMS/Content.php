<?php

namespace App\Models\CMS;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Content extends Model
{
    use SoftDeletes;

    const DETAIL_TYPE_BLOG = 'blog';
    const DETAIL_TYPE_PICTURE = 'picture';
    const DETAIL_TYPE_LINK = 'link';
    const DETAIL_TYPE_PAGE = 'page';

    /**
     * 对应表名
     *
     * @var string
     */
    public $table = 'cms_contents';

    public $fillable = [
        'status',
        'content_detail_type',
        'content_detail_id',
        'tags',
        'view_count',//新加入的
        'author'//新加入的
    ];

    public function getCategory(){
        $arr = [];
        foreach ($this->categorylabel as $item){
            $arr[] = $this->StringArrayToInt(explode('-',$item->category_path));
        }
        return $arr;
    }
    public function StringArrayToInt($a){
        $array= [];
        foreach($a as $v)
        {
            if($v){
                $array[]=(int)$v;
            }

        }
        return $array;
    }
    /**
     * 动态关联
     * @return mixed
     */
    public function content_detail()

    {
        return $this->morphTo()->withTrashed();
    }


    /**
     * @return mixed
     */
    public function category()
    {
        return $this->belongsToMany(Category::class, 'content_has_categories', 'content_id', 'category_id')->withTrashed();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function categorylabel(){
        return $this->hasMany(ContentHasCategory::class,'content_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function navigation()
    {
        return $this->hasOne(Navigation::class, 'point_content_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function content_blog()
    {
        return $this->belongsTo(Blog::class, 'id','content_id')->withTrashed();
    }
}
