<?php

namespace App\Models\CMS;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;

    /**
     * 对应表名
     *
     * @var string
     */
    public $table = 'cms_categories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'parent_id',
        'full_path',
    ];

    /**
     * 父级分类
     * @return mixed
     */
    public function parent()
    {
        return $this->hasOne(Category::class, 'id', 'parent_id');
    }
}
