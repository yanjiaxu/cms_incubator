<?php

namespace App\Models\CMS;

use Illuminate\Database\Eloquent\Model;

class ContentHasCategory extends Model
{

    public $table = 'content_has_categories';

    public $timestamps = false;

    public $fillable = [
        'content_id',
        'category_path',
        'category_id',
    ];
}
