/** When your routing table is too long, you can split it into small modules**/

import Layout from '@/layout'

const backendRouter = {
  path: '/backend',
  component: Layout,
  redirect: 'index',
  name: 'backend',
  meta: {
    title: '后台设置',
    icon: 'fa fa-server',
    roles:[
      '用户管理','权限管理'
    ]
  },
  children: [
    {
      path: 'index',
      component: () => import('@/views/admin/admin/index'),
      name: 'backend-index',
      meta: { title: '用户管理',icon: 'fa fa-user-circle' , roles:['用户管理']}
    },
    {
      path: 'role',
        component: () => import('@/views/admin/role/index'),
      name: 'role-index',
      meta: { title: '权限管理',icon: 'fa fa-key', roles:['权限管理'] }
    },
]
}

export default backendRouter
