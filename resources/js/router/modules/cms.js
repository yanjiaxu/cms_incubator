/** When your routing table is too long, you can split it into small modules**/

import Layout from '@/layout'

const cmsRouter = {
  path: '/cms',
  component: Layout,
  redirect: 'index',
  name: 'cms',
  meta: {
    title: '内容管理',
    icon: 'fa fa-wordpress',
    // roles:[
    //   '用户管理','权限管理'
    // ]
  },
  children: [
      {
          path: 'index',
          name: 'cms-category',
          meta: {
              title: '栏目管理',
              icon: 'fa fa-columns',
          },
          component: () => import('@/views/cms/category/index'),
      },
      {
          path: 'navigation',
          component: () => import('@/views/cms/navigation/index'),
          name: 'cms-navigation',
          meta: { title: '导航管理',icon: 'fa fa-circle' }
      },
    {
      path: 'assets',
      component: () => import('@/views/cms/assets/index'),
      name: 'cms-assets',
      meta: { title: '素材管理',icon: 'fa fa-file' }
    },
      {
          path: 'blog',
          component: () => import('@/views/cms/blog/index'),
          name: 'cms-blog',
          meta: { title: '文章管理',icon: 'fa fa-file-text-o' }
      },
      {
          path: 'picture',
          component: () => import('@/views/cms/picture/index'),
          name: 'cms-picture',
          meta: { title: '广告位管理',icon: 'fa fa-picture-o' }
      },
]
}

export default cmsRouter
