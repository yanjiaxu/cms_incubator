
/**
 * Model 数据源
 * 用于 单条记录的增删查改
 */
(function (root, factory) {
    // CommonJS
    if (typeof exports === 'object') {
        module.exports = factory();
    }
    // Browser global
    else {
        root.ListDataSource = factory();
    }

}
(this, function () {
    let _ = require('lodash');
    let request = require('../utils/request').default;
    // let defaultSettings = {
    //     url: '',
    //     dataKey: '',
    //     defaultAttributes: {},
    // };

    function ListDataSource(vm, settings) {
        this.vm = vm;
        this.settings = settings;
        this.debug = true;
        this.init();
    }

    ListDataSource.prototype = {
        vm: null,
        settings: null,
        id: 0,
        pageCount: 0,
        currentPage: 0,
        total: 0,
        /**
         *
         */
        init: function () {
            let self = this;
            self.items = [];
            _.assign(self, self.settings.attributes);
        },
        /**
         *
         * @param query
         * @param successCallback
         * @param failedCallback
         */
        fetch: function (params = {}, successCallback, failedCallback) {
            let self = this;
            let url = self.settings.url;
            request({
                url: url,
                method: 'get',
                params: params
            })
            .then(function (response) {
                self.items = [];
                if(response.data){
                    _.forEach(response.data, function (value, key) {
                        self.items.push(value);
                    });
                }else{
                    _.forEach(response, function (value, key) {
                        self.items.push(value);
                    });
                }


                if (response.meta){
                    if (response.meta.pagination){
                      self.total = Number.parseInt(response.meta.pagination.total);
                      self.currentPage = Number.parseInt(response.meta.pagination.current_page);
                      self.pageCount = Number.parseInt(response.meta.pagination.total_pages);
                    }else{
                      self.total = Number.parseInt(response.meta.total);
                      self.currentPage = Number.parseInt(response.meta.current_page);
                      self.pageCount = Number.parseInt(response.meta.last_page);
                    }
                }

                typeof successCallback === 'function' && successCallback();
            }).catch(function (error) {
                // Something happened in setting up the request that triggered an Error
                self.debug && console.log('Error', error.message);
                typeof failedCallback === 'function' && failedCallback();
            });
        }


    };


    /**
     * Vue Plugin
     */
    ListDataSource.install = function (Vue) {
        Vue.prototype.$listDataSource = function (options) {
            return new ListDataSource(this, options);
        }
    };
    return ListDataSource;
}));
