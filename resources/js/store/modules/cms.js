import { TagList } from '@/api/dataProvider'
import { fetchCategories } from '@/api/cms'

const state = {
    tags: [],
    category: [],
}

const mutations = {
    SET_TAG: (state, tags) => {
        state.tags = tags
    },
    SET_CATEGORY: (state, category) => {
        state.category = category
    },
}

const actions = {
    fetchTags ({commit}) {
        TagList().then((res) => {
            // console.log(res)
            commit('SET_TAG', res.data)
        })
    },
    fetchCategoryList ({commit, state}) {
        fetchCategories().then((res) => {
            // console.log(res)
            commit('SET_CATEGORY', res)
        })
    },
}

export default {
    namespaced: true,
    state,
    mutations,
    actions,
}
