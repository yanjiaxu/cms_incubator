const getters = {
    sidebar: state => state.app.sidebar,
    size: state => state.app.size,
    device: state => state.app.device,
    visitedViews: state => state.tagsView.visitedViews,
    cachedViews: state => state.tagsView.cachedViews,
    name: state => state.user.name,
    token: state => state.user.token,
    roles: state => state.user.roles,
    permission_routes: state => state.permission.routes,
    errorLogs: state => state.errorLog.logs,
    rolesList: state => state.permission.roles_list,
    permissionsList: state => state.permission.permissions_list,
    tags: state => state.cms.tags,
    categoryList:state => state.cms.category
}
export default getters
