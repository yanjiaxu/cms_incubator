import Vue from 'vue'

import Cookies from 'js-cookie'

import Element from 'element-ui'

import App from './App.vue'
import store from './store'
import router from './router'

import './permission' // permission control
import './utils/error-log' // error log

import * as filters from './filters' // global filters

Vue.use(Element, {
    size: Cookies.get('size') || 'medium' // set element-ui default size
})

/**
 * 自定义组件
 */
window.ModelDataSource = require('./libs/ModelDataSource')
Vue.use(ModelDataSource)
window.ListDataSource = require('./libs/ListDataSource')
Vue.use(ListDataSource)

// register global utility filters
Object.keys(filters).forEach(key => {
    Vue.filter(key, filters[key])
})


Vue.config.productionTip = false

new Vue({
    el: '#app',
    router,
    store,
    render: h => h(App)
})
