import request from '@/utils/request'

export function fetchCategories() {
    return request({
        url: '/api/cms/category',
        method: 'get',
    })
}

export function fetchAssets() {
    return request({
        url: '/api/cms/asset',
        method: 'get',
    })
}

export function getContentsByCategory(category) {
    return request({
        url: '/api/data-provider/get-content-by-category',
        method: 'post',
        params: { category: category }
    })
}
