import request from '@/utils/request'

export function PermissionList() {
    return request({
        url: '/api/data-provider/permission-list',
        method: 'get',
    })
}

export function RolesList() {
    return request({
        url: '/api/data-provider/roles-list',
        method: 'get',
    })
}

export function TagList() {
    return request({
        url: '/api/data-provider/tag-list',
        method: 'get',
    })
}

