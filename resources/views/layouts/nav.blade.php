<!-- ======= Header ======= -->
<header id="header" class="fixed-top header-transparent">
    <div class="container">

        <div class="logo float-left">
            <h1 class="text-light"><a href="/"><span>华泰宇轩</span></a></h1>
            <!-- Uncomment below if you prefer to use an image logo -->
            <!-- <a href="index.html"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->
        </div>

        <nav class="nav-menu float-right d-none d-lg-block">
            <ul>

                @foreach(\App\Repositories\WebRepository::getNav() as $item)
                    <li><a href="{{$item->point_url}}">{{$item->name}} <i class="la la-angle-down"></i></a></li>
                @endforeach

{{--                <li><a href="about.html">About Us</a></li>--}}
{{--                <li><a href="services.html">Services</a></li>--}}
{{--                <li><a href="portfolio.html">Portfolio</a></li>--}}
{{--                <li><a href="team.html">Team</a></li>--}}
{{--                <li><a href="blog.html">Blog</a></li>--}}
{{--                <li class="drop-down"><a href="">Drop Down</a>--}}
{{--                    <ul>--}}
{{--                        <li><a href="#">Drop Down 1</a></li>--}}
{{--                        <li class="drop-down"><a href="#">Drop Down 2</a>--}}
{{--                            <ul>--}}
{{--                                <li><a href="#">Deep Drop Down 1</a></li>--}}
{{--                                <li><a href="#">Deep Drop Down 2</a></li>--}}
{{--                                <li><a href="#">Deep Drop Down 3</a></li>--}}
{{--                                <li><a href="#">Deep Drop Down 4</a></li>--}}
{{--                                <li><a href="#">Deep Drop Down 5</a></li>--}}
{{--                            </ul>--}}
{{--                        </li>--}}
{{--                        <li><a href="#">Drop Down 3</a></li>--}}
{{--                        <li><a href="#">Drop Down 4</a></li>--}}
{{--                        <li><a href="#">Drop Down 5</a></li>--}}
{{--                    </ul>--}}
{{--                </li>--}}
{{--                <li><a href="contact.html">Contact Us</a></li>--}}
            </ul>
        </nav><!-- .nav-menu -->

    </div>
</header><!-- End Header -->
