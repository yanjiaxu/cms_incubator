<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCmsContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_contents', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('status',20)->comment('状态');
            $table->string('content_detail_type',20)->comment('类型');
            $table->integer('content_detail_id')->comment('关联ID');
            $table->string('tags')->nullable()->comment('标签');
            $table->integer('view_count')->default(0)->comment('点击量');
            $table->string('author')->nullable()->comment('作者');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_contents');
    }
}
