<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCmsPicturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_content_pictures', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title')->nullable()->comment('标题');
            $table->integer('assets_id')->comment('关联素材');
            $table->integer('content_id')->comment('关联内容');
            $table->string('point_url')->nullable()->comment('链接地址');
            $table->string('point_type',20)->nullable()->comment('导航类型');
            $table->integer('point_content_id')->default(0)->comment('关联内容');
            $table->integer('point_category_id')->default(0)->comment('栏目');
            $table->string('point_category_path')->nullable()->comment('导航全地址');
            $table->string('description')->nullable()->comment('介绍');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_content_pictures');
    }
}
