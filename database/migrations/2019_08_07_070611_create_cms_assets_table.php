<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCmsAssetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_assets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable()->comment('素材名称');
            $table->string('path')->comment('素材地址');
            $table->string('type',20)->comment('素材类型');
            $table->string('thumbnail_path')->nullable()->comment('缩略地址');
            $table->string('tags')->nullable()->comment('标签');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_assets');
    }
}
