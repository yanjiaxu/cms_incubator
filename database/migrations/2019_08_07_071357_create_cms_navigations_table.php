<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCmsNavigationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_navigations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->nullable()->comment('导航名称');
            $table->integer('parent_id')->default(0)->comment('上级ID');
            $table->integer('sort')->default(0)->comment('排序');
            $table->string('point_url')->nullable()->comment('链接地址');
            $table->string('point_type',20)->nullable()->comment('导航类型');
            $table->integer('point_content_id')->default(0)->comment('关联内容');
            $table->integer('point_category_id')->default(0)->comment('栏目');
            $table->string('point_category_path')->nullable()->comment('导航全地址');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_navigations');
    }
}
