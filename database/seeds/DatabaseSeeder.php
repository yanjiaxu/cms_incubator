<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        \App\Models\Admin::create([
            'username' => 'admin',
            'name' => '管理员',
            'password' => bcrypt('111111'),
        ]);
    }
}
