const mix = require('laravel-mix');

function resolve (dir) {
    return path.join(__dirname, dir)
}

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .sass('resources/sass/app.scss', 'public/css');



mix.webpackConfig({
    // output: {
    //     publicPath: "",
    //     chunkFilename: './js/bundles/[name].js',
    // },
    resolve: {
        alias: {
            'resources': resolve('resources'),
            '@': resolve('resources/js')
        }
    },
})


mix.browserSync({
    proxy: 'cms.com'
})
