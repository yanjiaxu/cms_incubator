<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('auth/login','AuthController@login');
Route::post('file/upload', 'FileController@fileUpload');
Route::group(['middleware' => ['auth:api']], function () {
    Route::get('auth/info','AuthController@info');

    /**
     * 后台用户管理
     */
    Route::resource('admin','AdminController', ['except' => ['create', 'edit']]);

    /**
     * 用户权限
     */
    Route::resource('role', 'RoleController', ['except' => ['create', 'edit']]);


    Route::group(['namespace' => 'CMS', 'prefix' => 'cms'], function () {

        /**
         * 栏目
         */
        Route::apiResource('category', 'CategoryController', ['except' => ['create', 'edit']]);
        /**
         * 导航管理
         */
        Route::apiResource('navigation', 'NavigationController', ['except' => ['create', 'edit']]);
        /**
         * 素材
         */
        Route::apiResource('asset', 'AssetController', ['except' => ['create', 'edit']]);
        /**
         * 文章
         */
        Route::apiResource('blog', 'ContentBlogController', ['except' => ['create', 'edit']]);
        /**
         * 广告位
         */
        Route::apiResource('picture', 'ContentPictureController', ['except' => ['create', 'edit']]);
    });
    Route::group(['prefix' => 'data-provider'], function () {
        Route::get('permission-list', 'DataProviderController@getPermission');                          //所有权限
        Route::get('roles-list', 'DataProviderController@getRole');                                       //所有角色
        Route::get('tag-list', 'DataProviderController@getTag');
        Route::post('get-content-by-category', 'DataProviderController@getContentsByCategory');
    });
});
